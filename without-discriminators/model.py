import torch
from torch import nn


class ResidualBlock(nn.Module):
    """
    Residual Block with instance normalization
    """
    def __init__(self, in_channels, out_channels):
        super().__init__()

        self.residual_block = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=False),
            nn.InstanceNorm2d(out_channels, affine=True, track_running_stats=True), nn.ReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=False),
            nn.InstanceNorm2d(out_channels, affine=True, track_running_stats=True))

    def forward(self, x):
        return x + self.residual_block(x)


class Resnet(nn.Module):
    def __init__(self, input_channels, conv_dim, repeat_num):
        super().__init__()

        layers = [
            nn.Conv2d(input_channels, conv_dim, kernel_size=7, stride=1, padding=3, bias=False),
            nn.InstanceNorm2d(conv_dim, affine=True, track_running_stats=True),
            nn.ReLU(inplace=True)
        ]

        # Down-sampling
        curr_dim = conv_dim
        for _ in range(2):
            layers.append(nn.Conv2d(curr_dim, curr_dim * 2, kernel_size=4, stride=2, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(curr_dim * 2, affine=True, track_running_stats=True))
            layers.append(nn.ReLU(inplace=True))
            curr_dim = curr_dim * 2

        # Bottleneck
        for _ in range(repeat_num):
            layers.append(ResidualBlock(in_channels=curr_dim, out_channels=curr_dim))

        # Up-sampling
        for _ in range(2):
            layers.append(nn.ConvTranspose2d(curr_dim, curr_dim // 2, kernel_size=4, stride=2, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(curr_dim // 2, affine=True, track_running_stats=True))
            layers.append(nn.ReLU(inplace=True))
            curr_dim = curr_dim // 2

        layers.append(nn.Conv2d(curr_dim, input_channels, kernel_size=7, stride=1, padding=3, bias=False))
        layers.append(nn.Sigmoid())

        self.model = nn.Sequential(*layers)

    def forward(self, x):
        return self.model(x)


import efficientunet


class EfficientUnet(nn.Module):
    def __init__(self, size=0, out_channels=1, concat_input=True, pretrained=True, freeze_encoder=True, sigmoid_output=True):
        super().__init__()

        assert 0 <= size <= 7, 'size must be in range [0, 7]'
        model_fn = getattr(efficientunet, f'get_efficientunet_b{size}')
        model = model_fn(out_channels=out_channels, concat_input=concat_input, pretrained=pretrained)
        model.encoder.requires_grad = not freeze_encoder
        self.model = model
        self.sigmoid_output = sigmoid_output

    def forward(self, x):
        if x.size(1) == 1:
            # convert grayscale to rgb
            # x = x.repeat(1, 3, 1, 1)
            x = x.expand(-1, 3, -1, -1)
        x = self.model(x)
        if self.sigmoid_output:
            x = torch.sigmoid(x)
        return x


from functools import reduce
from operator import __add__
from typing import List, Optional

import torch
import torch.nn.functional as F
from torch import Tensor, nn


class UNet(nn.Module):
    def __init__(
        self,
        channels_in: int = 1,
        channels_out: int = 1,
        bilinear: bool = True,
        layer_norm: bool = False,
        filters: List[int] = None,
    ) -> "UNet":

        super().__init__()

        if not filters:
            self.filters: List[int] = [64, 128, 256, 512, 1024]
        else:
            self.filters: List[int] = filters

        self.channels_in: int = channels_in
        self.channels_out: int = channels_out
        self.bilinear: bool = bilinear
        self.factor: int = 2 if bilinear else 1
        self.layer_norm: bool = layer_norm

        self.inc = DoubleConv(channels_in, self.filters[0])

        self.down1 = Down(self.filters[0], self.filters[1], self.layer_norm)
        self.down2 = Down(self.filters[1], self.filters[2], self.layer_norm)
        self.down3 = Down(self.filters[2], self.filters[3], self.layer_norm)
        self.down4 = Down(self.filters[3], self.filters[4] // self.factor)

        self.up1 = Up(self.filters[4], self.filters[3] // self.factor, self.bilinear)
        self.up2 = Up(self.filters[3], self.filters[2] // self.factor, self.bilinear)
        self.up3 = Up(self.filters[2], self.filters[1] // self.factor, self.bilinear)
        self.up4 = Up(self.filters[1], self.filters[0], bilinear)

        self.outc = OutConv(self.filters[0], self.channels_out, 16)

    def forward(self, x: Tensor) -> Tensor:
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)

        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)

        o = self.outc(x)

        return o


class DoubleConv(nn.Module):
    def __init__(self,
                 channels_in: int,
                 channels_out: int,
                 channels_mid: Optional[int] = None,
                 layer_norm: bool = False) -> "DoubleConv":

        super().__init__()

        self.layer_norm: bool = layer_norm

        if not channels_mid:
            channels_mid = channels_out

        self.double_conv = nn.Sequential(
            nn.Conv2d(channels_in, channels_mid, kernel_size=(3, 3), padding=(1, 1)),
            nn.BatchNorm2d(channels_mid),
            nn.ReLU(inplace=True),
            nn.Conv2d(channels_mid, channels_out, kernel_size=(3, 3), padding=(1, 1)),
            nn.BatchNorm2d(channels_out),
            nn.ReLU(inplace=True),
        )

    def forward(self, x: Tensor) -> Tensor:
        x = self.double_conv(x)

        if self.layer_norm:
            x = nn.LayerNorm(x.size()[1:])(x)

        return x


class Down(nn.Module):
    def __init__(self, channels_in: int, channels_out: int, layer_norm: bool = False) -> "Down":

        super().__init__()

        self.maxpool_conv = nn.Sequential(nn.MaxPool2d(2), DoubleConv(channels_in, channels_out, layer_norm=layer_norm))

    def forward(self, x: Tensor) -> Tensor:
        return self.maxpool_conv(x)


class Up(nn.Module):
    def __init__(self, channels_int: int, channels_out: int, bilinear: bool = True, layer_norm: bool = False) -> "Up":

        super().__init__()

        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True)
            self.conv = DoubleConv(channels_int, channels_out, channels_int // 2, layer_norm=layer_norm)
        else:
            self.up = nn.ConvTranspose2d(channels_int, channels_int // 2, kernel_size=(2, 2), stride=(2, 2))
            self.conv = DoubleConv(channels_int, channels_out, layer_norm=layer_norm)

    def forward(self, x1: Tensor, x2: Tensor) -> Tensor:
        x1 = self.up(x1)

        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2, diffY // 2, diffY - diffY // 2])

        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


SamePad2d = lambda ksize: nn.ZeroPad2d(reduce(__add__, [(k // 2 + (k - 2 * (k // 2)) - 1, k // 2)
                                                        for k in ksize[::-1]]))


class OutConv(nn.Module):
    def __init__(self, channels_in: int, channels_out: int, channels_mid: int) -> "OutConv":

        super(OutConv, self).__init__()

        self.out = nn.Sequential(
            SamePad2d(ksize=(3, 3)),
            nn.Conv2d(channels_in, channels_mid, kernel_size=(3, 3)),
            nn.ReLU(inplace=True),
            nn.Conv2d(channels_mid, channels_out, kernel_size=(1, 1)),
            nn.Sigmoid(),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.out(x)


def create_model(model_name, model_kwargs):
    if model_name == 'resnet':
        return Resnet(**model_kwargs)
    elif model_name == 'unet':
        return UNet(**model_kwargs)
    elif model_name == 'efficient-unet':
        return EfficientUnet(**model_kwargs)
    else:
        raise ValueError(f'Model {model_name} is not supported')
