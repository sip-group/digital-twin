import os

import hydra
import torch
from hydra.core.hydra_config import HydraConfig
from omegaconf import DictConfig, OmegaConf
from torch import nn, optim
from torch.nn import functional as F
from torchvision.utils import make_grid
from tqdm.autonotebook import tqdm, trange

import wandb
from data_utils import create_dataloaders
from model import create_model

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def train_val_loop(model, dataloader, phase, epoch_num=0, optimizer=None, cfg=None):
    loss_fn = getattr(nn, cfg.training.loss_fn)()
    n_images = cfg.logging.n_images
    from_name, _, to_name = cfg.common.direction.partition('2')

    model.train(mode=(phase == 'train'))
    for i, (x, to) in enumerate(tqdm(dataloader, leave=False, desc=f'{phase}:{epoch_num}')):
        x, to = x.to(device), to.to(device)

        if cfg.training.dequantization:
            x = (x * 255. + torch.rand_like(x, device=device)) / 256.

        with torch.set_grad_enabled(phase == 'train'):
            out = model(x)
            loss = loss_fn(out, to)
            if phase == 'train':
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            logs = {
                'loss': loss,
                'l1_loss': F.l1_loss(out, to),
                'l2_loss': F.mse_loss(out, to),
                'step': i + epoch_num * len(dataloader)
            }
            if i % cfg.logging.image_freq == 0:
                x, to, out = x[:n_images], to[:n_images], out[:n_images]
                stacked = torch.stack((x, to, out), dim=1).view(-1, *out.shape[1:])
                logs['image'] = wandb.Image(make_grid(stacked, nrow=3),
                                            caption=f'{from_name} vs {to_name} vs estimation')

            if cfg.logging.include_accuracy:
                accuracy = (out.round() == to).float().mean().item()
                logs = {**logs, 'accuracy': accuracy, 'hamming_error': 1 - accuracy}

            wandb.log({f"{phase}/{k}": v for k, v in logs.items()})


def train_model(model, train_loader, eval_loader, cfg):
    optimizer = optim.AdamW(model.parameters())
    for epoch in trange(cfg.training.n_epochs):
        for loader, phase in zip([train_loader, eval_loader], ['train', 'eval']):
            train_val_loop(model, dataloader=loader, phase=phase, epoch_num=epoch, optimizer=optimizer, cfg=cfg)
        torch.save(model.state_dict(), f'{cfg.training.save_dir}/{epoch:03}.pt')


@hydra.main(version_base=None, config_path="conf", config_name="config")
def main(cfg: DictConfig) -> None:
    assert cfg.common.direction in {'printed2template', 'template2printed'}
    task_name = HydraConfig.get().runtime.choices.model
    cfg.common.project += "_" + cfg.common.direction
    cfg.common.name = f'{cfg.common.direction}_{task_name}'
    cfg.logging.include_accuracy = cfg.common.direction == 'printed2template'
    cfg.training.save_dir += cfg.common.name
    os.makedirs(cfg.training.save_dir, exist_ok=True)
    wandb.init(name=task_name,
               project=cfg.common.project,
               config=OmegaConf.to_container(cfg, resolve=True, throw_on_missing=True),
               settings=wandb.Settings(start_method='thread'))
    with open(f'{cfg.training.save_dir}/config.yaml', 'w') as f:
        OmegaConf.save(config=cfg, f=f)

    train_dataloader, eval_dataloader = create_dataloaders(cfg)
    code_model = create_model(cfg.model.name, cfg.model.params).to(device)
    train_model(model=code_model, train_loader=train_dataloader, eval_loader=eval_dataloader, cfg=cfg)

    wandb.finish()


if __name__ == '__main__':
    main()
