from pathlib import Path

import hydra
import numpy as np
import torch
import tqdm
from cleanfid import fid
from omegaconf import DictConfig
from torchvision.utils import save_image
from tqdm.autonotebook import tqdm, trange

import wandb
from data_utils import create_dataloaders
from data_utils import save_image as my_save_image
from data_utils import tensor2im
from evaluation import (calculate_stat_per_epoch, hamming_error, mse_error,
                        structural_similarity)
from model import create_model

template_datased_fid_name = '1x1_template'
printed_datased_fid_name = '1x1_hpi76_printed'


@torch.no_grad()
@hydra.main(version_base=None, config_path='models/printed2template_unet', config_name="config")
def main(cfg: DictConfig):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    _, dataloader = create_dataloaders(cfg, return_name=True)
    code_model = create_model(cfg.model.name, cfg.model.params).to(device)

    estimation_path = Path('estimation') / cfg.common.name
    estimation_path.mkdir(exist_ok=True, parents=True)
    fid_dataset_name = template_datased_fid_name if cfg.common.direction == 'printed2template' else printed_datased_fid_name
    to_real_path_fn = lambda x: dataloader.dataset.to_dir / x.name

    mse, ssim, fids, hamming = [], [], [], []
    for i in trange(cfg.training.n_epochs):
        i = 116
        code_model.load_state_dict(torch.load(f"{cfg.training.save_dir}/{i:03}.pt", map_location=device))
        code_model.eval()

        for _, (x, to, names) in enumerate(tqdm(dataloader, leave=False, desc=f'Test loop')):
            x = x.to(device)
            to = to.to(device)
            if cfg.training.dequantization:
                x = (x * 255. + torch.rand_like(x, device=device)) / 256.

            out = code_model(x)

            for image, name in zip(out, names):
                image = tensor2im(image.unsqueeze(0) * 2 - 1)
                my_save_image(image, estimation_path / name)
        break
        fids.append(
            fid.compute_fid(estimation_path,
                            dataset_name=fid_dataset_name,
                            dataset_res='na',
                            dataset_split='custom',
                            num_workers=cfg.training.num_workers))
        fake_images = list(estimation_path.glob('*.png'))
        mse.append(calculate_stat_per_epoch(fake_images, to_real_path_fn, mse_error))
        ssim.append(calculate_stat_per_epoch(fake_images, to_real_path_fn, structural_similarity))
        if cfg.common.direction == 'printed2template':
            hamming.append(calculate_stat_per_epoch(fake_images, to_real_path_fn, hamming_error))

    # print(
    #     f'FID: {min(enumerate(fids), key=lambda x: x[1])}. mse: {min(enumerate(mse), key=lambda x: x[1])}. ssim: {max(enumerate(ssim), key=lambda x: x[1])}'
    # )
    # if cfg.common.direction == 'printed2template':
    #     print(f'hamming: {min(enumerate(hamming), key=lambda x: x[1])}')


if __name__ == '__main__':
    main()
