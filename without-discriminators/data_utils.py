from pathlib import Path

import numpy as np
import torch
from PIL import Image
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms
from torchvision.transforms import functional as TF


class CodeDataset(Dataset):
    def __init__(self, from_dir, to_dir=None, return_name=False, transform=None):
        self.from_dir = from_dir if isinstance(from_dir, Path) else Path(from_dir)
        self.to_dir = to_dir if isinstance(to_dir, Path) else Path(to_dir)
        self.return_name = return_name
        self.transform = transform
        self.modified_images = list(self.from_dir.glob('*.png'))

    def __len__(self):
        return len(self.modified_images)

    def __getitem__(self, i):
        modified_image_path = self.modified_images[i]
        modified_image = Image.open(modified_image_path)
        original_image = TF.to_tensor(Image.open(self.to_dir / modified_image_path.name)) if self.to_dir else None

        if self.transform is not None:
            modified_image = self.transform(modified_image)

        return (modified_image, original_image, modified_image_path.name) if self.return_name else \
            (modified_image, original_image)


def get_transforms():
    return transforms.Compose([
        transforms.ToTensor(),
        # transforms.Normalize([0.5], [0.5])
    ])


def create_dataloaders(cfg, **kwargs):
    data_dir = Path(cfg.data.data_dir)
    template_dir_train = data_dir / cfg.data.template_dir_train
    printed_dir_train = data_dir / cfg.data.printed_dir_train
    template_dir_eval = data_dir / cfg.data.template_dir_eval
    printed_dir_eval = data_dir / cfg.data.printed_dir_eval
    if cfg.common.direction == 'printed2template':
        from_dir_train, to_dir_train = printed_dir_train, template_dir_train
        from_dir_eval, to_dir_eval = printed_dir_eval, template_dir_eval
    else:
        from_dir_train, to_dir_train = template_dir_train, printed_dir_train
        from_dir_eval, to_dir_eval = template_dir_eval, printed_dir_eval
    codes_transforms = get_transforms()
    train_dataset = CodeDataset(from_dir_train, to_dir_train, transform=codes_transforms, **kwargs)
    eval_dataset = CodeDataset(from_dir_eval, to_dir_eval, transform=codes_transforms, **kwargs)

    train_dataloader = DataLoader(train_dataset,
                                  batch_size=cfg.training.batch_size,
                                  num_workers=cfg.training.num_workers,
                                  pin_memory=True,
                                  shuffle=True)
    eval_dataloader = DataLoader(eval_dataset,
                                 batch_size=cfg.training.batch_size,
                                 num_workers=cfg.training.num_workers,
                                 pin_memory=True)
    return train_dataloader, eval_dataloader


def tensor2im(input_image, imtype=np.uint8):
    """"Converts a Tensor array into a numpy image array.

    Parameters:
        input_image (tensor) --  the input image tensor array
        imtype (type)        --  the desired type of the converted numpy array
    """
    if not isinstance(input_image, np.ndarray):
        if isinstance(input_image, torch.Tensor):  # get the data from a variable
            image_tensor = input_image.data
        else:
            return input_image
        image_numpy = image_tensor[0].clamp(-1.0, 1.0).cpu().float().numpy()  # convert it into a numpy array
        if image_numpy.shape[0] == 1:  # grayscale to RGB
            image_numpy = np.tile(image_numpy, (3, 1, 1))
        image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + 1) / 2.0 * 255.0  # post-processing: tranpose and scaling
    else:  # if it is a numpy array, do nothing
        image_numpy = input_image
    return image_numpy.astype(imtype)


def save_image(image_numpy, image_path, aspect_ratio=1.0):
    """Save a numpy image to the disk

    Parameters:
        image_numpy (numpy array) -- input numpy array
        image_path (str)          -- the path of the image
    """

    image_pil = Image.fromarray(image_numpy)
    h, w, _ = image_numpy.shape

    if aspect_ratio is None:
        pass
    elif aspect_ratio > 1.0:
        image_pil = image_pil.resize((h, int(w * aspect_ratio)), Image.BICUBIC)
    elif aspect_ratio < 1.0:
        image_pil = image_pil.resize((int(h / aspect_ratio), w), Image.BICUBIC)
    image_pil.save(image_path)
