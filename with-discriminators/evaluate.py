
import seaborn as sns
from matplotlib import pyplot as plt

sns.set()

import os
import random
from collections import defaultdict
from functools import partial
from itertools import chain
from pathlib import Path

import numpy as np
import pandas as pd
import torch
from cleanfid import fid
from tqdm.autonotebook import tqdm, trange

import util.util as util
from data import create_dataset
from models import create_model
from options.test_options import TestOptions
from util import html
from util.visualizer import save_images

fid.tqdm = tqdm

from collections import ChainMap

import cv2
import skimage
from joblib import Parallel, delayed
from PIL import Image
from skimage.filters import threshold_local, threshold_multiotsu
from skimage.metrics import mean_squared_error as mse
from skimage.metrics import structural_similarity as ssim

N_IMAGES = 144 * 4 # 15
SEED = 228
BLOCK_SIZE = '1x1'
LOAD_SIZE = 256
DATAROOT = f'./../data/i2i/{BLOCK_SIZE}/HPI76/'
DATASET_NAME = 'template2printed'

opposite_dataset = {
    "template": "printed",
    "printed": "template"
}
template_datased_fid_name = '1x1_template'
printed_datased_fid_name = '1x1_hpi76_printed'

def fprint(x, filename="results.txt", mode="a"):
    with open(filename, mode) as f:
        print(x, file=f)
    print(x)

def create_options(checkpoint_name):
    DIRECTION = "AtoB"
    NAME = checkpoint_name.stem
    fprint(f"Current experiment name is: {NAME}")
    if 'turbo' in NAME.lower():
        MODEL = 'turbo'
        MODEL_KWARGS = ''
        assert DIRECTION == "AtoB"
    elif 'cycle' in NAME.lower():
        MODEL = 'cycle_gan'
        MODEL_KWARGS = ''
        assert DIRECTION == "AtoB"
    else:
        MODEL = 'cut' 
        MODEL_KWARGS = '--CUT_mode CUT' 
        if 'printed2template' in NAME.lower():
            DIRECTION = "BtoA"
    if 'unet' in NAME.lower():
        MODEL_KWARGS += ' --netG unet_256'
    
    # if 'wgan' in NAME.lower():
    #     MODEL_KWARGS += ' --gan_mode wgangp'

    PHASE = 'val'
    opt = TestOptions(f"""
                --dataroot {DATAROOT}{DATASET_NAME} --direction {DIRECTION} --name {NAME} --model {MODEL} {MODEL_KWARGS}
                --num_test {N_IMAGES}
                --input_nc 1 --output_nc 1
                --no_flip --preprocess none --load_size {LOAD_SIZE} --batch_size 1 --eval
                --serial_batches
                --phase {PHASE}
                """).parse()


    opt.num_threads = 0   # test code only supports num_threads = 1
    opt.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
    opt.display_id = -1   # no visdom display; the test code saves the results to a HTML file

    return opt

def generate_images(opt, weights):
    for epoch in tqdm(weights):
        opt.epoch = epoch
        torch.manual_seed(SEED)
        np.random.seed(SEED)
        random.seed(SEED)
        
        dataset = create_dataset(opt)  # create a dataset given opt.dataset_mode and other options
        model = create_model(opt)      # create a model given opt.model and other options
        
        # create a webpage for viewing the results
        web_dir = os.path.join(opt.results_dir, BLOCK_SIZE, opt.name, opt.phase, '{}_{}'.format(opt.phase, opt.epoch))  # define the website directory
        print('creating web directory', web_dir)
        webpage = html.HTML(web_dir, 'Experiment = %s, Phase = %s, Epoch = %s' % (opt.name, opt.phase, opt.epoch))

        for i, data in enumerate(tqdm(dataset, total=N_IMAGES, leave=False)):
            if i == 0:
                model.data_dependent_initialize(data)
                model.setup(opt)               # regular setup: load and print networks; create schedulers
                model.parallelize()
                if opt.eval:
                    model.eval()
            if i >= opt.num_test:  # only apply our model to opt.num_test images.
                break
            model.set_input(data)  # unpack data from data loader
            model.test()           # run inference
            visuals = model.get_current_visuals()  # get image results
            img_path = model.get_image_paths()     # get image paths
            save_images(webpage, visuals, img_path, width=opt.display_winsize, use_wandb=opt.use_wandb)
        webpage.save()  # save the HTML


def calculate_fid(opt, weights, compare_dataset_name, fake_dataset_type, fake_name):
    template_fid_path = Path(f'{DATAROOT}{DATASET_NAME}/valA')
    printed_fid_path = Path(f'{DATAROOT}{DATASET_NAME}/valB')

    # template_printed_fid = fid.compute_fid(template_fid_path, printed_fid_path, num_workers=0)
    template_printed_fid = 303.9226520919982

    if not fid.test_stats_exists(template_datased_fid_name, mode='clean'):
        fid.make_custom_stats(template_datased_fid_name, template_fid_path)
    if not fid.test_stats_exists(printed_datased_fid_name, mode='clean'):
        fid.make_custom_stats(printed_datased_fid_name, printed_fid_path)

    translation_name = f'{opposite_dataset[fake_name]}2{fake_name}'
    result_dir = f'./{opt.results_dir}{BLOCK_SIZE}/{opt.name}/{opt.phase}/'
    fid_scores = {}
    for epoch in tqdm(weights):
        path = f'{result_dir}{opt.phase}_{epoch}/images/fake_{fake_dataset_type}'
        fid_scores[epoch] = fid.compute_fid(path, dataset_name=compare_dataset_name, dataset_res='na', dataset_split='custom', num_workers=40)

    df = pd.DataFrame(fid_scores.items(), columns=['epoch', 'score'])
    best_fid_row = df.iloc[df['score'].idxmin()].to_string().replace('\n', ' ')
    fprint(f"Lowest FID for {translation_name}: {best_fid_row}")
    df.to_csv(f'{result_dir}fid_{translation_name}.csv', index=False)

    fig, ax = plt.subplots(figsize=(10, 6))
    label = f'FID({compare_dataset_name}, fake_{fake_name})'
    sns.lineplot(x='epoch', y='score', data=df, label=label)
    sns.lineplot(y=template_printed_fid, x=df['epoch'], label=f'FID({template_datased_fid_name}, {printed_datased_fid_name})')
    plt.title(f"FID score between train {compare_dataset_name} and test {opposite_dataset[fake_name]} ➝ {fake_name} fakes")
    plt.xlabel("epoch")
    plt.ylabel("FID")
    plt.legend()
    plt.savefig(f"{result_dir}{label.replace(' ', '')}.png")
    plt.show()

def read_image(path):
    return np.array(Image.open(path))[:, :, 0] # .convert('L')

thresholds = [x for x in dir(skimage.filters) if 'threshold_' in x and x not in ['threshold_local', 'threshold_multiotsu']]
thresholds = [getattr(skimage.filters, x) for x in thresholds] + [partial(threshold_local, block_size=15), partial(threshold_multiotsu, classes=2)]
def hamming_error(img_fake, img_orig):
    errors = []
    for threshold_fn in thresholds:
            try:
                thresh = threshold_fn(image=img_fake)
                binary = ((img_fake > thresh) * 255).astype(np.uint8)
                errors.append(cv2.countNonZero(cv2.bitwise_xor(binary, img_orig)) / img_fake.size)
            except Exception as e:
                print(threshold_fn, e)
    return min(errors)

def mse_error(img_fake, img_orig):
    return mse(img_fake, img_orig)

def structural_similarity(img_fake, img_orig):
    return ssim(img_orig, img_fake)

def calculate_stat_per_image(fake_image_path, to_real_path_fn, statistic_fn):
    fake_image = read_image(fake_image_path)
    real_image = read_image(to_real_path_fn(fake_image_path))
    value = None
    try:
        value = statistic_fn(fake_image, real_image)
    except Exception as e:
        print(fake_image_path, statistic_fn, e)
    return {fake_image_path.stem: value}

def calculate_stat_per_epoch(fake_paths, to_real_path_fn, statistic_fn):
    stats = Parallel(n_jobs=-1)(delayed(calculate_stat_per_image)(fake_image_path, to_real_path_fn, statistic_fn)  for fake_image_path in tqdm(fake_paths, leave=False))
    stats = dict(ChainMap(*stats))
    return np.mean([x for x in stats.values() if x is not None])


def calculate_stats(opt, weights, dataset_type, dataset_name, statistic_fn, best="lowest", description="error"):
    stats = {}
    result_dir = f'./{opt.results_dir}{BLOCK_SIZE}/{opt.name}/{opt.phase}/'

    for epoch in tqdm(weights):
        image_path = Path(f'{result_dir}{opt.phase}_{epoch}/images/')
        fake_paths = list((image_path / f'fake_{dataset_type}').iterdir())
        real_directory = image_path / f'real_{dataset_type}'
        stats[epoch] = calculate_stat_per_epoch(fake_paths, lambda fake_image_path: real_directory / fake_image_path.name, statistic_fn)
    
    df = pd.DataFrame(stats.items(), columns=['epoch', description]).sort_values(by='epoch')
    if best == "lowest":
        best_row = df[description].idxmin()
    else:
        best_row = df[description].idxmax()
    best_row = df.iloc[best_row].to_string().replace('\n', ' ')
    label = f'{description}(real_{dataset_name},fake_{dataset_name})'.replace(' ', '')
    fprint(f"{best} {label}: {best_row}")
    df.to_csv(f'{result_dir}{label}.csv', index=False)

    fig, ax = plt.subplots(figsize=(10, 6))
    sns.lineplot(x='epoch', y=description, data=df, label=label)
    plt.title(f"{description} between real test {dataset_name} and test {opposite_dataset[dataset_name]} ➝ {dataset_name} fakes")
    plt.xlabel("epoch")
    plt.ylabel(description)
    plt.legend()
    plt.savefig(f"{result_dir}{label}.png")
    plt.show()

def measure_templates(opt, weights):
    calculate_stats(opt, weights, opt.direction[0], 'template', hamming_error, best="lowest", description="hamming_error")
    # printed2template fid
    calculate_fid(opt, weights, compare_dataset_name=template_datased_fid_name, fake_dataset_type=opt.direction[0], fake_name='template')
    

def measure_printed(opt, weights):
    calculate_stats(opt, weights, opt.direction[-1], 'printed', mse_error, best="lowest", description="mse_error")
    calculate_stats(opt, weights, opt.direction[-1], 'printed', structural_similarity, best="highest", description="structural_similarity")
    # template2printed fid
    calculate_fid(opt, weights, compare_dataset_name=printed_datased_fid_name, fake_dataset_type=opt.direction[-1], fake_name='printed')


def main():
    CHECKPOINTS_DIR = Path('checkpoints')
    CHECKPOINTS_NAME = sorted([x for x in CHECKPOINTS_DIR.iterdir() if BLOCK_SIZE in x.name])
    # CHECKPOINTS_NAME = [x for x in CHECKPOINTS_NAME if 'turbo' in x.stem.lower()]
    # CHECKPOINTS_NAME = [x for x in (CHECKPOINTS_NAME) if ('no_flip' in x.stem.lower()) and ('wgangp' in x.stem.lower())]
    # CHECKPOINTS_NAME.remove(CHECKPOINTS_DIR / "all_hacks_400_1x1_HPI76_8bit_template2printed_CYCLE")
    result_path = Path("results/1x1")
    for checkpoint_name in tqdm(CHECKPOINTS_NAME, desc="checkpoints"):
        # if '2printed' not in checkpoint_name.stem:
        #     continue
        # if (result_path / checkpoint_name.stem).exists():
        #     continue
        opt = create_options(checkpoint_name)
        weights = sorted({int(x.stem.partition('_')[0]) for x in checkpoint_name.glob('*_net_G*') if 'latest' not in x.stem}) #+ ['latest']
        generate_images(opt, weights)
        if opt.model in ('cycle_gan', 'turbo') or opt.direction == 'BtoA':
            measure_templates(opt, weights)
        if opt.model in ('cycle_gan', 'turbo') or opt.direction == 'AtoB':
            measure_printed(opt, weights)

        fprint(f"{checkpoint_name} done\n\n")

if __name__ == "__main__":
    main()
