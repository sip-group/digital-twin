# Digital twis

Official implementation of "Digital twins of physical printing-imaging channel" paper presented during [2022 IEEE International Workshop on Information Forensics and Security (WIFS)](https://wifs2022.utt.fr/) and TBD journal.

## Overview
We present a new approach to model a printing-imaging channel using a machine learning-based "digital twin" for copy detection patterns (CDP). The CDP are considered as modern anti-counterfeiting features in multiple applications. Our digital twin is formulated within the information-theoretic framework of TURBO, using variational approximations of mutual information for both encoder and decoder in the bidirectional exchange of information. This model extends various architectural designs, including paired pix2pix and unpaired CycleGAN, for image-to-image translation. Applicable to any type of printing and imaging devices, the model needs only training data comprising digital templates sent to a printing device and data acquired by an imaging device. The data can be paired, unpaired, or hybrid, ensuring architectural flexibility and scalability for multiple practical setups. We explore the influence of various architectural factors, metrics, and discriminators on the overall system's performance in generating and predicting printed CDP from their digital versions and vice versa. We also performed a comparison with several state-of-the-art methods for image-to-image translation applications.


For more details, please refer to our IEEE WIFS 2022 conference [paper](https://arxiv.org/abs/2210.17420) "Digital twins of physical printing-imaging channel" or [slides](./2022_WIFS_slides.pdf).


![scheme](./scheme.png)

## Requirements
The code is meant to be run on Python 3.9 or later version. Code dependencies are specified in the `requirements.txt` file.


## Usage
We divide our codebase into two parts:
1. Training without discriminator
2. Training with discriminator

More details about each part are provided in the corresponding folders.


## Citing us
```bibtex
@inproceedings{Belousov2022wifs,
	title        = {Digital twins of physical printing-imaging channel},
	author       = {Belousov, Yury and Pulfer, Brian and Chaban, Roman and Tutt, Joakim and Taran, Olga and Holotyak, Taras and Voloshynovskiy, Slava},
	year         = {2022},
	month        = {December},
	booktitle    = {2022 IEEE International Workshop on Information Forensics and Security (WIFS)},
	address      = {Shanghai, China},
	pages        = {1--6},
	doi          = {10.1109/WIFS55849.2022.9975439}
}
```

## License
TBD